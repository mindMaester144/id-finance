package com.example.finance.controllers;

import com.example.finance.entities.Request;
import com.example.finance.models.RequestCreateDto;
import com.example.finance.models.RequestUpdateDto;
import com.example.finance.models.StatisticsDto;
import com.example.finance.models.StatusDto;
import com.example.finance.services.RequestService;
import com.example.finance.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/request")
public class RequestController {
    private final RequestService requestService;
    private final UserService userService;

    public RequestController(RequestService requestService, UserService userService) {
        this.requestService = requestService;
        this.userService = userService;
    }

    @GetMapping("all")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Page<Request>> getAll(@RequestParam(defaultValue = "0") @Min(0) int page,
                                                @RequestParam(defaultValue = "10") @Min(1) @Max(50) int size) {
        return ResponseEntity.ok(requestService.findAll(PageRequest.of(page, size)));
    }

    @PatchMapping("update")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<Request> update(@Valid @RequestBody RequestUpdateDto requestUpdateDto) {
        Optional<Request> requestOptional = requestService.findById(requestUpdateDto.getId());

        if (requestOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(requestService.updateStatus(requestUpdateDto));
    }

    @GetMapping("statistics")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<StatisticsDto> getStatistics() {
        return ResponseEntity.ok(requestService.getStatistics());
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<Long> create(@Valid @RequestBody RequestCreateDto requestDto, Principal principal) {
        Request request = requestService.create(requestDto, userService.findByUsername(principal.getName()));

        return ResponseEntity.ok(request.getId());
    }

    @GetMapping("status/{id}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<StatusDto> getStatus(@PathVariable("id") Long id, Principal principal) {
        Optional<Request> requestOptional = requestService.findById(id);

        if (requestOptional.isEmpty() || !requestOptional.get().getAuthor().getUsername().equals(principal.getName())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(convertToDto(requestOptional.get()));
    }

    private StatusDto convertToDto(Request request) {
        StatusDto statusDto = new StatusDto();
        statusDto.setStatus(request.getStatus().name());

        return statusDto;
    }
}
