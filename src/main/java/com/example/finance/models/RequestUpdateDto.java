package com.example.finance.models;

import com.example.finance.entities.enums.Status;
import jdk.jfr.DataAmount;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RequestUpdateDto {
//    @NotNull
    private Long id;
//    @NotNull
    private Status status;
}
