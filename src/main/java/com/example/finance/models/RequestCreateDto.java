package com.example.finance.models;

import com.example.finance.utils.validateion.DateConstraint;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class RequestCreateDto {
    @NotNull
    @JsonProperty("request")
    private String text;

    @Positive
    private Float bid;

    @DateConstraint(format = "yyyy-mm-dd")
    @JsonProperty("due_date")
    private String dueDate;
}
