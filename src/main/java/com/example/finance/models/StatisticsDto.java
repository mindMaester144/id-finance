package com.example.finance.models;

import lombok.Data;

@Data
public class StatisticsDto {
    private Long doneCount;
    private Long deniedCount;
}
