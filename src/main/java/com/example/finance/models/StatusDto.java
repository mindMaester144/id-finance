package com.example.finance.models;

import lombok.Data;

@Data
public class StatusDto {
    private String status;
}
