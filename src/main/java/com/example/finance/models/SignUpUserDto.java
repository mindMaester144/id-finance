package com.example.finance.models;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class SignUpUserDto {
    @NotNull
    @Size(min = 4, max = 20)
    @Pattern(regexp = "[A-Za-z0-9]+")
    private String username;
    @NotNull
    @Email
    private String email;
    @NotNull
    @Size(min = 4, max = 20)
    @Pattern(regexp = "[\\w\\[\\]`!@#$%\\^&*()={}:;<>+\'-]*")
    private String password;
}
