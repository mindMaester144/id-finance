package com.example.finance.models;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthToken {
    private String token;
}
