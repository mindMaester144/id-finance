package com.example.finance.services;

import com.example.finance.entities.User;
import com.example.finance.entities.enums.Role;
import com.example.finance.models.SignUpUserDto;
import com.example.finance.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptEncoder;

    @Autowired
    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptEncoder) {
        this.userRepository = userRepository;
        this.bCryptEncoder = bCryptEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.isEnabled(), user.isAccountNonExpired(), user.isCredentialsNonExpired(),
                user.isAccountNonLocked(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.toString()));
        });
        return authorities;
    }

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);

        return list;
    }

    public void delete(long id) {
        userRepository.deleteById(id);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Optional<User> findById(long id) {
        return userRepository.findById(id);
    }

    public User save(SignUpUserDto signUpUserDTO) {
        User newUser = userRepository.findByUsername(signUpUserDTO.getUsername());

        if (newUser != null) {
            return null;
        }
        newUser = new User();

        newUser.setUsername(signUpUserDTO.getUsername());
        newUser.setEmail(signUpUserDTO.getEmail());
        newUser.setPassword(bCryptEncoder.encode(signUpUserDTO.getPassword()));
        newUser.setRoles(setRole(Role.ROLE_USER));
        newUser.setActive(true);

        return userRepository.save(newUser);
    }

    public boolean isUsernameFree(String username) {
        User user = userRepository.findByUsername(username);
        return user == null;
    }

    public boolean isEmailFree(String email) {
        List<User> users = findAll();

        for (User user : users) {
            if (user.getEmail().equals(email)) {
                return false;
            }
        }

        return true;
    }


    public HashSet<Role> setRole(Role role) {
        HashSet<Role> roles = new HashSet<>();
        roles.add(role);

        return roles;
    }
}
