package com.example.finance.services;

import com.example.finance.entities.Request;
import com.example.finance.entities.User;
import com.example.finance.entities.enums.Status;
import com.example.finance.models.RequestCreateDto;
import com.example.finance.models.RequestUpdateDto;
import com.example.finance.models.StatisticsDto;
import com.example.finance.repositories.RequestRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

@Service
public class RequestService {
    private final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public Page<Request> findAll(PageRequest pageRequest) {
        return requestRepository.findAll(pageRequest);
    }

    public Optional<Request> findById(Long id) {
        return requestRepository.findById(id);
    }

    public Request create(RequestCreateDto requestDto, User author) {

        Request request = Request.builder()
                .text(requestDto.getText())
                .bid(requestDto.getBid())
                .dueDate(Timestamp.valueOf(requestDto.getDueDate() + " 00:00:00.00"))
                .author(author)
                .status(Status.PENDING)
                .build();

        return requestRepository.save(request);
    }

    public StatisticsDto getStatistics() {
        StatisticsDto result = new StatisticsDto();

        result.setDoneCount(requestRepository.count(Example.of(Request.builder().status(Status.DONE).build(),
                ExampleMatcher.matchingAny())));
        result.setDeniedCount(requestRepository.count(Example.of(Request.builder().status(Status.DENIED).build(),
                ExampleMatcher.matchingAny())));

        return result;
    }

    public Request updateStatus(RequestUpdateDto requestUpdateDto) {
        Request request = requestRepository.getById(requestUpdateDto.getId());

        request.setStatus(requestUpdateDto.getStatus());

        return requestRepository.save(request);
    }
}
