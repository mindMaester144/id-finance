package com.example.finance.utils.validateion;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateValidator.class)
@Documented
public @interface DateConstraint {
    String message() default "Wrong date format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String format();
}
