package com.example.finance.utils.validateion;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidator implements ConstraintValidator<DateConstraint, String> {
    private String dateFormat;

    @Override
    public void initialize(DateConstraint acceptedValues) {
        this.dateFormat = acceptedValues.format();
    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        DateFormat sdf = new SimpleDateFormat(this.dateFormat);
        sdf.setLenient(false);
        try {
            sdf.parse(field);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
