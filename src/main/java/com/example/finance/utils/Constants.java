package com.example.finance.utils;

public class Constants {
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 10*24*60*60;
    public static final String SIGNING_KEY = "mACzeTA";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scopes";
}
