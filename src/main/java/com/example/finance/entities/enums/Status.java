package com.example.finance.entities.enums;

public enum Status {
    PENDING, DONE, DENIED
}
