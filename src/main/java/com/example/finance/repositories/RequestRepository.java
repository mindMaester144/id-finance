package com.example.finance.repositories;

import com.example.finance.entities.Request;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {
    @Override
    <S extends Request> long count(Example<S> example);
}