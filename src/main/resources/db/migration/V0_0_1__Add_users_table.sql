CREATE TABLE users
(
    user_id             SERIAL PRIMARY KEY,
    username            varchar(255)            NOT NULL UNIQUE,
    email               varchar(255)            NOT NULL UNIQUE,
    password            varchar(255)            NOT NULL,
    active              boolean                 NOT NULL,
    blocked             boolean                 NOT NULL,
    created_at          timestamp DEFAULT now(),
    updated_at          timestamp DEFAULT now()
);

CREATE TABLE user_role
(
    user_id    bigint unsigned         NOT NULL,
    role_name  varchar(255)            NOT NULL,
    granted_at timestamp DEFAULT now() NOT NULL,
    CONSTRAINT user_role_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES users (user_id) MATCH FULL
        ON UPDATE NO ACTION ON DELETE NO ACTION,

    CONSTRAINT user_role_role_name_user_id_uniq UNIQUE (user_id, role_name)
);