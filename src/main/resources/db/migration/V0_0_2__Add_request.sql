CREATE TABLE request
(
    request_id        SERIAL PRIMARY KEY,

    author_id bigint unsigned NOT NULL,
    text              varchar(255)    NOT NULL,
    bid               float           NOT NULL,
    due_date          timestamp       NOT NULL,
    status            varchar(32)     NOT NULL,

    CONSTRAINT request_requested_user_id_fkey FOREIGN KEY (author_id)
        REFERENCES users (user_id) MATCH FULL
        ON UPDATE NO ACTION ON DELETE CASCADE
);